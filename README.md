# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 1.0
* This repo is to simulate how can we populate the contact list for local json file, add and edit new object into json file.

### How do I get set up? ###

* Summary of set up
* Configuration: After clone this project, please run the pod install to update the pod file
* Dependencies: IQKeyboardManager, Material, Validator, TTAttributeLabel, DZEmptyDataSet
* Setup: Change UIColor_Hex_Swift and SwiftyJSON to swift 4.2
* Lastly, enjoy :)

### About the app ###
* User can track their expenses on this app by selecting the pre-configure category (food, transport, utility, other) and add their expenses.
* The app will automatically sum the expenses base on their category