import UIKit

public extension UICollectionView {
  public func registerCellClass <CellClass: UICollectionViewCell> (_ cellClass: CellClass.Type) {
    register(cellClass, forCellWithReuseIdentifier: cellClass.description())
  }

  public func registerCellNibForClass(_ cellClass: AnyClass) -> UINib {
    let classNameWithoutModule = cellClass
      .description()
      .components(separatedBy: ".")
      .dropFirst()
      .joined(separator: ".")

    let nib = UINib(nibName: classNameWithoutModule, bundle: nil)
    
    register(nib,
             forCellWithReuseIdentifier: classNameWithoutModule)
    
    return nib
  }
}
