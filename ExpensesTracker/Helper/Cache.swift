//
//  Cache.swift
//  hubhome-ios
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 17/01/2020.
//  Copyright © 2020 UEM Sunrise. All rights reserved.
//

import Foundation
import WebKit

struct Cache {
    
    static var CustomerId: Int = 0
    static var Token: String?
    static var CustomerUniqueId: Int = 0
    static var Name: String?
    static var Email: String?
    static var IcNumber: String?
    static var MobileNumber: String?
    static var TressionId: String?
    static var ExpensesData: [[String: AnyObject]] = [[:]]
    
  
    static func clear() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    }
    
    static func setExpensesData(value: Any, keyName: String) {
        UserDefaults.standard.setValue(value, forKey: keyName)
    }
    
    static func getExpensesData(keyName: String) -> [Any]? {
        return UserDefaults.standard.array(forKey: keyName)
    }
        
    static func setUser(value: String, keyName: String) {
          UserDefaults.standard.set(value, forKey: keyName)
    }
    
    static func setFirstRun(value: Bool, keyName: String) {
        UserDefaults.standard.setValue(value, forKey: keyName)
    }
    
    static func getFirstRun(keyName: String) -> Bool? {
        return UserDefaults.standard.bool(forKey: keyName)
    }
    
    static func setFilter(value: Any, keyName: String) {
        UserDefaults.standard.set(value, forKey: keyName)
    }
    
    static func getFilter(keyName: String) -> [Any]? {
        return UserDefaults.standard.array(forKey: keyName)
    }
        
    static func getUser(keyName: String) -> String? {
        return UserDefaults.standard.string(forKey: keyName)
    }
    
    static func removeUser(keyName: String) {
        UserDefaults.standard.removeObject(forKey: keyName)
    }
    
    static func removeExpensesData(keyName: String) {
        UserDefaults.standard.removeObject(forKey: keyName)
    }

}
