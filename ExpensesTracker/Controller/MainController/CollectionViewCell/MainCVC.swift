//
//  MainCVC.swift
//  ExpensesTracker
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 26/09/2021.
//

import UIKit
import Library

protocol MainCVCViewModelInputs {
    
}

protocol MainCVCViewModelOutputs {

    var catTitle: Box<String?> { get }
    var imageName: Box<String?> { get }
    var currentExp: Box<Double> { get }


    var sectionInset: Box<UIEdgeInsets> { get }
    var itemSpacing: Box<CGFloat> { get }
    
    var currentIndex: Box<Int?> { get }

    
}

protocol MainCVCViewModelType {
    var inputs: MainCVCViewModelInputs { get }
    var outputs: MainCVCViewModelOutputs { get }
}

class MainCVCViewModel: MainCVCViewModelInputs, MainCVCViewModelOutputs, MainCVCViewModelType {
    
    let catTitle: Box<String?> = Box(nil)
    let imageName: Box<String?> = Box("")
    let currentExp: Box<Double> = Box(0.0)

    
    let sectionInset: Box<UIEdgeInsets> = Box(UIEdgeInsets(top: 8, left: 24, bottom: 8, right: 24))
    let itemSpacing: Box<CGFloat> = Box(8.0)
    
    let currentIndex: Box<Int?> = Box(nil)

    
    var inputs: MainCVCViewModelInputs { return self }
    var outputs: MainCVCViewModelOutputs { return self }
    
    init() {
        
    }
    
}

class MainCVC: UICollectionViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var iconImgView: UIImageView!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var currentExpLbl: UILabel!
    
    let viewModel: MainCVCViewModelType = MainCVCViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListener()

    }

    
    override var intrinsicContentSize: CGSize {
        let size = containerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        return size
    }

    fileprivate func setupView() {
    
        containerView.layer.cornerRadius = 10
    }
    
    fileprivate func setupListener() {
        
        /*viewModel.outputs.cellStyle.bind { [weak self]  value in
            guard let strongSelf = self else { return }
            
            switch value {
            case .main:
                strongSelf.containerView.backgroundColor = UIColor.white
                strongSelf.indicatorImgView.alpha = 1
                strongSelf.subDescLbl.alpha = 1
            case .details:
                strongSelf.containerView.backgroundColor = UIColor.white
                strongSelf.indicatorImgView.alpha = 0
                strongSelf.subDescLbl.alpha = 0
            }

        }*/
        
        viewModel.outputs.catTitle.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.categoryLbl.text = value
            
        }
        
        viewModel.outputs.imageName.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.iconImgView.image = UIImage(named: value)
            
        }

        viewModel.outputs.currentExp.bind { [weak self] value in
            guard let strongSelf = self else { return }
            
            if value == 0.0 {
                strongSelf.currentExpLbl.text = "RM 0.0"
            } else {
                strongSelf.currentExpLbl.text = "RM \(value)"

            }
            
            
        }

        
    }
    
}

// MARK: View Layout
extension MainCVC: ValueCell {
    func configureWith(value: MainCVCViewModelType) {
        viewModel.outputs.catTitle.value = value.outputs.catTitle.value
        viewModel.outputs.imageName.value = value.outputs.imageName.value
        viewModel.outputs.currentExp.value = value.outputs.currentExp.value
        viewModel.outputs.itemSpacing.value = value.outputs.itemSpacing.value
        viewModel.outputs.sectionInset.value = value.outputs.sectionInset.value
        viewModel.outputs.currentIndex.value = value.outputs.currentIndex.value


    }
      
}

