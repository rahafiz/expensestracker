//
//  MainVCDS.swift
//  EpfFinder
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 09/05/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation
import Library
import UIKit

class MainVCDS: ValueCellDataSource {
    enum Section: Int {
        case List
        
    }
    
    override init() {
        super.init()
    }
    
    
    func set(list: MainTVCViewModelType?) {
        let section = Section.List.rawValue
        
        self.clearValues(section: section)
        
        if let list = list {
            self.set(
                values: [list],
                cellClass: MainTVC.self,
                inSection: section
            )
        }
        
    }
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as MainTVC, value as MainTVCViewModelType):
            cell.configureWith(value: value)
        default:
            assertionFailure("Unrecognized combo: \(cell), \(value)")
        }
    }
}
