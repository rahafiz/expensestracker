//
//  MainTVCDS.swift
//  ExpensesTracker
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 26/09/2021.
//

import Foundation
import Library
import UIKit

class MainTVCDS: ValueCellDataSource {
    enum Section: Int {
        case Card
    }
    
    override init() {
        super.init()
    }
    
    
    func set(card: [MainCVCViewModelType]?) {
        let section = Section.Card.rawValue
        
        self.clearValues(section: section)
        
        if let card = card {
            self.set(
                values: card,
                cellClass: MainCVC.self,
                inSection: section
            )
        }
        
    }
    
    
    override func configureCell(collectionCell cell: UICollectionViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as MainCVC, value as MainCVCViewModelType):
            cell.configureWith(value: value)
        default:
            assertionFailure("")
        }
    }
    
}
