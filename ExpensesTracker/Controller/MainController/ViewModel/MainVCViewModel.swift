//
//  MainVCViewModel.swift
//  EpfFinder
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 09/05/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation

protocol MainVCViewModelInputs {
  
}

protocol MainVCViewModelOutputs {
   
    var catCardVM: Box<MainTVCViewModelType?> { get }
}

protocol MainVCViewModelType {
    var inputs: MainVCViewModelInputs { get }
    var outputs: MainVCViewModelOutputs { get }
}

class MainVCViewModel: MainVCViewModelInputs, MainVCViewModelOutputs, MainVCViewModelType {
   
    let catCardVM: Box<MainTVCViewModelType?> = Box(nil)
    
    var inputs: MainVCViewModelInputs { return self }
    var outputs: MainVCViewModelOutputs { return self }
    
    init() {
        
    }
    
}
