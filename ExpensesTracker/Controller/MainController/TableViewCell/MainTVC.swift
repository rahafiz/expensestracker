//
//  MainTVC.swift
//  EpfFinder
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 09/05/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import UIKit
import Library

protocol MainTVCDelegate: class {
    
    func mainTVCDelegate(_vc: MainTVC, selectedContent: MainCVCViewModelType)
}

protocol MainTVCViewModelInputs {
    
}

protocol MainTVCViewModelOutputs {
    
    var cellStyle: Box<MainTVCViewModel.cellStyle> { get }
    
    var collectionVM: Box<[MainCVCViewModelType]?> { get }

    var title: Box<String?> { get }
    var name: Box<String> { get }
    var distance: Box<Double> { get }
    var desc: Box<String?> { get }
    var subDesc: Box<String?> { get }
    var fax: Box<String?> { get }
    var lat: Box<Double> { get }
    var long: Box<Double> { get }

    var sectionInset: Box<UIEdgeInsets> { get }
    var itemSpacing: Box<CGFloat> { get }
    
    var currentIndex: Box<Int?> { get }
    
    var customCollectionViewHeight: Box<CGFloat?> { get }

    
}

protocol MainTVCViewModelType {
    var inputs: MainTVCViewModelInputs { get }
    var outputs: MainTVCViewModelOutputs { get }
}

class MainTVCViewModel: MainTVCViewModelInputs, MainTVCViewModelOutputs, MainTVCViewModelType {
    
    
    enum cellStyle: String {
        case main = "main"
        case details = "details"
    }

    let cellStyle: Box<MainTVCViewModel.cellStyle> = Box(.main)
    
    let collectionVM: Box<[MainCVCViewModelType]?> = Box(nil)

    let title: Box<String?> = Box(nil)
    let name: Box<String> = Box("a")
    let distance: Box<Double> = Box(0.0)
    let desc: Box<String?> = Box(nil)
    let subDesc: Box<String?> = Box(nil)
    let fax: Box<String?> = Box(nil)
    let lat: Box<Double> = Box(0.0)
    let long: Box<Double> = Box(0.0)
    
    let sectionInset: Box<UIEdgeInsets> = Box(UIEdgeInsets(top: 8, left: 24, bottom: 8, right: 24))
    let itemSpacing: Box<CGFloat> = Box(8.0)
    
    let currentIndex: Box<Int?> = Box(nil)
    
    let customCollectionViewHeight: Box<CGFloat?> = Box(nil)

    
    var inputs: MainTVCViewModelInputs { return self }
    var outputs: MainTVCViewModelOutputs { return self }
    
    init() {
        
    }
    
}

class MainTVC: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var sectionTitleLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var csCollectionViewHeight: NSLayoutConstraint!
    
    let viewModel: MainTVCViewModelType = MainTVCViewModel()
    var dataSource = MainTVCDS()
    weak var delegate: MainTVCDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListener()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override var intrinsicContentSize: CGSize {
        let size = containerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        return size
    }

    fileprivate func setupView() {
        
        selectionStyle = .none
        collectionView.registerCellNibForClass(MainCVC.self)


        collectionView.delegate = self
        collectionView.dataSource = dataSource
        
        calculateHomeMenuCardCell()
    
    }
    
    fileprivate func setupListener() {
        
        viewModel.outputs.collectionVM.bind { [weak self] value in
            guard let strongSelf = self else { return }
            
            strongSelf.dataSource.set(card: value)
            strongSelf.collectionView.reloadData()
            strongSelf.collectionView.layoutIfNeeded()
            
            /*let cellSize = strongSelf.calculateHomeMenuCardCell
            strongSelf.viewModel.outputs.calculatedItemSize.value = cellSize
            strongSelf.setupDefaultCollectionViewLayout(cellSize: cellSize, scrollDirection: .horizontal, itemsPerRow: 2)*/
        }
        
        viewModel.outputs.title.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.sectionTitleLbl.text = value

        }
        
        viewModel.outputs.customCollectionViewHeight.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.csCollectionViewHeight.constant = value

        }

        
    }
    
    func calculateHomeMenuCardCell() {
       let recipeCellIdentifier = String(describing: MainCVC.self)
       let recipeCellNib = UINib.init(nibName: recipeCellIdentifier,
                                          bundle: nil)
       let homeMenuCVC = recipeCellNib.instantiate(withOwner: self, options: nil).first as? MainCVC
       homeMenuCVC?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0)
       
       let homeMenuCVCVM = MainCVCViewModel()
       homeMenuCVCVM.outputs.catTitle.value = "Transaction"
       
       homeMenuCVC?.configureWith(value: homeMenuCVCVM)
       homeMenuCVC?.setNeedsDisplay()
       homeMenuCVC?.layoutIfNeeded()
       homeMenuCVC?.layoutSubviews()
       
       /*let totalWidth = UIScreen.main.bounds.width - (viewModel.outputs.sectionInset.value.left + viewModel.outputs.sectionInset.value.right + viewModel.outputs.itemSpacing.value)
       let cellWidth = totalWidth / 4
       homeMenuCVC?.frame = CGRect(x: 0, y: 0, width: cellWidth, height: cellWidth)*/
       
       let totalWidth =  UIScreen.main.bounds.width - (viewModel.outputs.sectionInset.value.left + viewModel.outputs.sectionInset.value.right + viewModel.outputs.itemSpacing.value)
        let cellWidth = totalWidth / 3
       let cellSize = CGSize.init(width: cellWidth, height: homeMenuCVC?.intrinsicContentSize.height ?? 0)
       csCollectionViewHeight.constant = cellSize.height
       
       //TODO: setup snapping flow layout
       setupSnappingCollectionViewLayout(cellSize: cellSize)
       
   }
    
    fileprivate func setupSnappingCollectionViewLayout(cellSize: CGSize) {
        csCollectionViewHeight.constant = cellSize.height + viewModel.outputs.sectionInset.value.top + viewModel.outputs.sectionInset.value.bottom
        
        let snappingFlowLayout = SnappingCollectionViewLayout.init()
        snappingFlowLayout.scrollDirection = .horizontal
        snappingFlowLayout.minimumInteritemSpacing = viewModel.outputs.itemSpacing.value
        snappingFlowLayout.minimumLineSpacing = viewModel.outputs.itemSpacing.value
        snappingFlowLayout.sectionInset = viewModel.outputs.sectionInset.value
        snappingFlowLayout.itemSize = cellSize
        snappingFlowLayout.spacingLeft = 8
        
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.reloadData()
        collectionView.collectionViewLayout = snappingFlowLayout
        self.layoutIfNeeded()
    }
    
}

//MARK: CollectionView Delegate
extension MainTVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
     
        /*if let cell = cell as? homeMenuCVC {
            cell.delegate = self
        }*/
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInsets = viewModel.outputs.sectionInset.value
        
        /*if section == ParkingSessionTVCDS.Section.AddSession.rawValue {
            if let sessions = viewModel.outputs.sessions.value, sessions.count > 0 {
                edgeInsets = UIEdgeInsets(top: edgeInsets.top, left: -16, bottom: edgeInsets.bottom, right: edgeInsets.right)
            } else {
                edgeInsets = UIEdgeInsets(top: edgeInsets.top, left: -16, bottom: edgeInsets.bottom, right: edgeInsets.right)
            }
        }*/
        return edgeInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let contents = viewModel.outputs.collectionVM.value else { return }
        if contents.count > 0 {
            let selectedContent = contents[indexPath.row]
            delegate?.mainTVCDelegate(_vc: self, selectedContent: selectedContent)
        
        }

    }

}

// MARK: View Layout
extension MainTVC: ValueCell {
    func configureWith(value: MainTVCViewModelType) {
        viewModel.outputs.cellStyle.value = value.outputs.cellStyle.value
        viewModel.outputs.title.value = value.outputs.title.value
        viewModel.outputs.itemSpacing.value = value.outputs.itemSpacing.value
        viewModel.outputs.sectionInset.value = value.outputs.sectionInset.value
        viewModel.outputs.currentIndex.value = value.outputs.currentIndex.value
        viewModel.outputs.collectionVM.value = value.outputs.collectionVM.value
        viewModel.outputs.customCollectionViewHeight.value = value.outputs.customCollectionViewHeight.value

    }
      
}

