//
//  MainVC.swift
//  ExpensesTracker
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 26/09/2021.
//

import UIKit
import SVProgressHUD
import CoreLocation

class MainVC: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel: MainVCViewModelType = MainVCViewModel()
    var dataSource = MainVCDS()
    var locationManager = CLLocationManager()
    var searchActive : Bool = false

    // Original lists
    var allServices: [MainTVCViewModel] = []

    private var currentCoordinate: CLLocationCoordinate2D?

    fileprivate var cellHeightsDict = [IndexPath: CGFloat]()
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setContainerView(self.view)
        hideKeyboardWhenTappedAround()


        // Do any additional setup after loading the view.
        setupView()
        setupListener()
        setupNavBar()
        
        
        if let expensesData = Cache.getExpensesData(keyName: "expenses_data") {
            
            let tempCatCardVM = MainTVCViewModel()
            tempCatCardVM.outputs.title.value = "Expenses Category"
            var tempCatCardVMs = [MainCVCViewModel]()
            
            for category in expensesData {
                
                let collection = MainCVCViewModel()
                let categoryDictData = category as! [String: AnyObject]
                
                collection.outputs.imageName.value = categoryDictData["imageName"] as? String
                collection.outputs.catTitle.value = categoryDictData["category"] as? String
                collection.outputs.currentExp.value = categoryDictData["expenses"] as? Double ?? 0.0
                
                tempCatCardVMs.append(collection)
            }
            
            tempCatCardVM.outputs.collectionVM.value = tempCatCardVMs
            tempCatCardVM.outputs.customCollectionViewHeight.value = 250
            viewModel.outputs.catCardVM.value = tempCatCardVM
            
        } else {
            
            var categoryArray = [Any]()
            
            for i in 0..<4 {
                var categoryDict = [String: AnyObject]()
                
                if i == 0 {
                    categoryDict["category"] = "Food" as AnyObject
                    categoryDict["imageName"] = "food" as AnyObject
                    categoryDict["expenses"] = 0.0 as AnyObject
                }
                if i == 1 {
                    
                    categoryDict["category"] = "Transport" as AnyObject
                    categoryDict["imageName"] = "transport" as AnyObject
                    categoryDict["expenses"] = 0.0 as AnyObject
  
                }
                if i == 2 {
                    
                    categoryDict["category"] = "Utility" as AnyObject
                    categoryDict["imageName"] = "utility" as AnyObject
                    categoryDict["expenses"] = 0.0 as AnyObject

                }
                if i == 3 {
                    
                    categoryDict["category"] = "Other" as AnyObject
                    categoryDict["imageName"] = "other" as AnyObject
                    categoryDict["expenses"] = 0.0 as AnyObject

                }
                
                categoryArray.append(categoryDict)
            }
            
            Cache.setExpensesData(value: categoryArray, keyName: "expenses_data")
            
            
            let tempCatCardVM = MainTVCViewModel()
            tempCatCardVM.outputs.title.value = "Expenses Category"
            var tempCatCardVMs = [MainCVCViewModel]()
            
            for category in categoryArray {
                
                let collection = MainCVCViewModel()
                let categoryDictData = category as! [String: AnyObject]
                
                collection.outputs.imageName.value = categoryDictData["imageName"] as? String
                collection.outputs.catTitle.value = categoryDictData["category"] as? String
                collection.outputs.currentExp.value = categoryDictData["expenses"] as? Double ?? 0.0
                
                tempCatCardVMs.append(collection)
            }
            
            tempCatCardVM.outputs.collectionVM.value = tempCatCardVMs
            tempCatCardVM.outputs.customCollectionViewHeight.value = 250
            viewModel.outputs.catCardVM.value = tempCatCardVM
            
        }

    }
    
    private func beginLocationUpdates(locationManager: CLLocationManager) {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)

    }

    
    private func setupView() {
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none

    }
    
    private func setupNavBar() {
        
    }

    
    func setupListener() {
        
        tableView.registerCellNibForClass(MainTVC.self)
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        
        viewModel.outputs.catCardVM.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            DispatchQueue.main.async {
                strongSelf.dataSource.set(list: value)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
    }
    
   
    func showGeneralExpensesPopup(category: String) {
        
        let vcIdentifier = String(describing: GeneralExpensesPopupVC.self)
        guard let vc = UIViewController.vc(
                  "GeneralPopup",
                  identifier: vcIdentifier
                  ) as? GeneralExpensesPopupVC else {
                      return
            }
        
        vc.delegate = self
        
        
        DispatchQueue.main.async {
            self.presentPopUpController(
                                    tapDismiss: true,
                                    forError: false,
                                    customHeight: 250,
                                    controller: { () -> UIViewController? in
                                        
                                        vc.viewModel.outputs.category.value = category
                                        return vc
                            }, completion: nil)

        }

    }

}

//MARK: GeneralExpensesPopupVC Delegate
extension MainVC: GeneralExpensesPopupVCDelegate {
    func generalExpensesPopupVCDidYes(_ vc: GeneralExpensesPopupVC, description: String, amount: Double, category: String) {
        
        if let expensesData = Cache.getExpensesData(keyName: "expenses_data") {
            
            var categoryArray = [Any]()
            var finalAmount = 0.0

            for expenses in expensesData {
                var categoryDictData = expenses as! [String: AnyObject]
                
                if categoryDictData["category"] as! String == category {
                    let actualExpenses = categoryDictData["expenses"] as! Double
                    finalAmount = (actualExpenses + amount)
                    categoryDictData["expenses"] =  finalAmount as AnyObject
                    
                }
                
                categoryArray.append(categoryDictData)
            }
            
            Cache.removeExpensesData(keyName: "expenses_data")
            Cache.setExpensesData(value: categoryArray, keyName: "expenses_data")
            
            vc.dismiss(animated: true, completion: {
                
                guard let catCardVM = self.viewModel.outputs.catCardVM.value else { return }
                guard var catCardVMs = catCardVM.outputs.collectionVM.value else { return }
                
                for collection in catCardVMs {
                    
                    if category == collection.outputs.catTitle.value {
                        collection.outputs.currentExp.value = finalAmount
                        break
                    }
                    
                    catCardVMs.append(collection)
                }

                catCardVM.outputs.collectionVM.value = catCardVMs
                self.viewModel.outputs.catCardVM.value = catCardVM
            })
            
        }
    }
    
    func generalExpensesPopupVCpDidNo(_ vc: GeneralExpensesPopupVC) {
        vc.dismiss(animated: true, completion: nil)
    }
    
    
}

//MARK: Tableview Cell Delegate
extension MainVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeightsDict[indexPath] {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        cellHeightsDict[indexPath] = cell.frame.height
        
        if let cell = cell as? MainTVC {
            cell.delegate = self
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
}

//MARK: MainTVCDelegate
extension MainVC: MainTVCDelegate {
    func mainTVCDelegate(_vc: MainTVC, selectedContent: MainCVCViewModelType) {
        
        showGeneralExpensesPopup(category: selectedContent.outputs.catTitle.value ?? "")
    }
}

//MARK: API call method
extension MainVC {
    /*func postOffices(mobileType: String, lan: String, version: String) {
        SVProgressHUD.show()
        _ = Api.postOffices(mobileType: mobileType, lan: lan , version: version) {[weak self] (result) in
                SVProgressHUD.dismiss()
            
                 guard let strongSelf = self else { return }
                 if let value = result.value {
                    DispatchQueue.main.async {
                        strongSelf.handlePostOfficesAPI(response: value)
                    }
                 } else if let error = result.error {
                     strongSelf.handleError(
                         error: error,
                         inlineBlock: nil,
                         overlayBlock: nil,
                         noneBlock: nil
                     )
                 }
             }
    }*/
    
}


