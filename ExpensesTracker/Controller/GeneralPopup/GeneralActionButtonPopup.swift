//
//  GeneralActionButtonPopup.swift
//  hubhome-ios
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 30/01/2020.
//  Copyright © 2020 UEM Sunrise. All rights reserved.
//

import UIKit

protocol GeneralActionButtonPopupViewModelInputs {
}

protocol GeneralActionButtonPopupViewModelOutputs {
    var popUpType: Box<GeneralActionButtonPopupViewModel.PopUpType> { get }
    var actionType: Box<GeneralActionButtonPopupViewModel.ActionType> { get }
    
    
    var title: Box<String?> { get }
    var titleFont: Box<UIFont> { get }
    var titleTextColor: Box<UIColor> { get }
    
    var btnRightTitle: Box<String?> { get }
    var btnLeftTitle: Box<String?> { get }
    
    
}

protocol GeneralActionButtonPopupViewModelType {
    var inputs: GeneralActionButtonPopupViewModelInputs { get }
    var outputs: GeneralActionButtonPopupViewModelOutputs { get }
}

class GeneralActionButtonPopupViewModel: GeneralActionButtonPopupViewModelInputs, GeneralActionButtonPopupViewModelOutputs, GeneralActionButtonPopupViewModelType {
    
    enum PopUpType: Int {
        case image = 1
        case animation = 2
    }
    
    enum ActionType: Int {
        case okButton = 1
        case flatButton = 2
    }
    
    
    let popUpType: Box<GeneralActionButtonPopupViewModel.PopUpType> = Box(.image)
    let actionType: Box<GeneralActionButtonPopupViewModel.ActionType> = Box(.flatButton)
    
    let title: Box<String?> = Box(nil)
    let titleFont: Box<UIFont> = Box(UIFont.raleway(size: 16))
    let titleTextColor: Box<UIColor> = Box(UIColor("#636363"))
    
    let btnRightTitle: Box<String?> = Box(nil)
    let btnRightFont: Box<UIFont> = Box(UIFont.ralewayBold(size: 16))
    let btnRightTextColor: Box<UIColor> = Box(UIColor.contentBlack())
    
    let btnLeftTitle: Box<String?> = Box(nil)
    let btnLeftFont: Box<UIFont> = Box(UIFont.ralewayBold(size: 16))
    let btnLeftTextColor: Box<UIColor> = Box(UIColor.contentBlack())

    var inputs: GeneralActionButtonPopupViewModelInputs { return self }
    var outputs: GeneralActionButtonPopupViewModelOutputs { return self }
}

protocol GeneralActionButtonPopupDelegate: class {
    func generalActionButtonPopupDidYes(
        _ vc: GeneralActionButtonPopup
    )
    
    func generalActionButtonPopupDidNo(
        _ vc: GeneralActionButtonPopup
    )
}

class GeneralActionButtonPopup: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    let viewModel: GeneralActionButtonPopupViewModelType = GeneralActionButtonPopupViewModel()
    weak var delegate: GeneralActionButtonPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupView()
        setupListener()

    }
    
    fileprivate func setupView() {
        
        containerView.layer.cornerRadius = 4.0
        
        leftBtn.layer.cornerRadius = 4.0
        rightButton.clipsToBounds = true
        rightButton.layer.cornerRadius = 4.0
        rightButton.layer.borderWidth = 2
        rightButton.layer.borderColor = UIColor("#3da5e7").cgColor
        leftBtn.backgroundColor = UIColor("#3da5e7")
        leftBtn.clipsToBounds = true

     }
    
      fileprivate func setupListener() {
                      
            viewModel.outputs.title.bind { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.titleLbl.text = $0
            }
            
            viewModel.outputs.titleFont.bind { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.titleLbl.font = $0
            }
            
            viewModel.outputs.titleTextColor.bind { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.titleLbl.textColor = $0
            }
            
            viewModel.outputs.btnRightTitle.bind { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.rightButton.setTitle($0, for:.normal)
            }
        
            viewModel.outputs.btnLeftTitle.bind { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.leftBtn.setTitle($0, for:.normal)
            }

        }
    
    @IBAction func clickedLeftBtn(_ sender: Any) {
          
        delegate?.generalActionButtonPopupDidYes(self)
    }
      
    @IBAction func clickedRightBtn(_ sender: Any) {
          
          delegate?.generalActionButtonPopupDidNo(self)

    }
    
}
