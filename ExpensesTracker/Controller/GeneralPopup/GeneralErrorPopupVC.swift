//
//  GeneralErrorPopup.swift
//  boostapp
//
//  Created by Cheah Bee Kim on 12/9/17.
//  Copyright © 2017 BOOST. All rights reserved.
//

import UIKit

protocol GeneralErrorVCDelegate: class {
    func generalErrorPopup(viewController: GeneralErrorPopupVC,
                           onPress okBtn: UIButton)
}

struct GeneralErrorVCViewModel {
    var header: String?     = NSLocalizedString("common_error_header",
                                                comment: "")
    var title: String?      = NSLocalizedString("common_error_title",
                                                comment: "")
    var content: String?    = NSLocalizedString("common_error_content",
                                                comment: "")
    
    var cancelImg: UIImage? = UIImage(named: "close")?
        .withRenderingMode(.alwaysTemplate)
    var cancelTintColor = UIColor.contentBlack()
    var showCancelBtn: Bool = false
    
    var okImg: UIImage? = UIImage(named: "tick_white")?
        .withRenderingMode(.alwaysTemplate)
    var okTintColor = UIColor.white
    var imageIconStr: String = "ic_sad"

    //
    var cancelHandler: (() -> Void)?
    var okHandler: (() -> Void)?
}

class GeneralErrorPopupVC: UIViewController {
    @IBOutlet weak var sadLogo: UIImageView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var okBtn: UIButton!
    
    @IBAction func onPressOkBtn(sender: UIButton) {
        guard let okHandler = viewModel.okHandler else {
            if let strongDelegate = self.delegate {
                strongDelegate.generalErrorPopup(
                    viewController: self,
                    onPress: sender
                )
            } else {
                self.dismiss(animated: true,
                             completion: nil)
            }
            
            return
        }
        
        okHandler()
    }
    
    @IBAction func onPressCloseBtn(sender: UIButton) {
        guard let cancelHandler = viewModel.cancelHandler else {
            dismiss(animated: true, completion: nil)
            
            return
        }
        
        cancelHandler()
    }

    var viewModel: GeneralErrorVCViewModel = GeneralErrorVCViewModel()
    weak var delegate: GeneralErrorVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup
    func setupViews() {
        sadLogo.image = UIImage(named: viewModel.imageIconStr)?
            .withRenderingMode(.alwaysOriginal)
        sadLogo.contentMode = .scaleToFill
        
        headerLbl.textColor = UIColor.black
        headerLbl.font = UIFont.dinProBold(size: 17)
        headerLbl.numberOfLines = 0
        
        titleLbl.textColor = UIColor.gray
        titleLbl.font = UIFont.dinPro(size: 16)
        titleLbl.numberOfLines = 0
        
        contentLbl.font = UIFont.t15()
        contentLbl.lineBreakMode = .byWordWrapping
        
        headerLbl.text = viewModel.header
        titleLbl.text = viewModel.title
        contentLbl.attributedText = ThemeManager.htmlAttributedString(
            text: "",
            regularFont: UIFont.bst_b1(),
            boldFont: UIFont.bst_b1_bold(),
            textColor: UIColor.contentBlack()
        )
        //contentLbl.text = contentText
        
        cancelBtn.clipsToBounds = true
        cancelBtn.layer.cornerRadius = cancelBtn.frame.height / 2.0
        cancelBtn.backgroundColor = UIColor.boostBackgroundGray()
        cancelBtn.setImage(
            viewModel.cancelImg,
            for: .normal
        )
        cancelBtn.tintColor = viewModel.cancelTintColor
        cancelBtn.isHidden = !viewModel.showCancelBtn
        
        okBtn.clipsToBounds = true
        okBtn.layer.cornerRadius = okBtn.frame.height / 2.0
        okBtn.backgroundColor = UIColor("#3DA5E7")
        okBtn.setImage(
            viewModel.okImg,
            for: .normal
        )
        okBtn.tintColor = viewModel.okTintColor
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
