//
//  GeneralExpensesPopupVC.swift
//  ExpensesTracker
//
//  Created by Wan Rahafiz Bin Wan Abdul Rahim on 26/09/2021.
//

import UIKit

protocol GeneralExpensesPopupVCViewModelInputs {
}

protocol GeneralExpensesPopupVCViewModelOutputs {
    
    var category: Box<String?> { get }
    var description: Box<String?> { get }
    var amount: Box<Double?> { get }
    
}

protocol GeneralExpensesPopupVCViewModelType {
    var inputs: GeneralExpensesPopupVCViewModelInputs { get }
    var outputs: GeneralExpensesPopupVCViewModelOutputs { get }
}

class GeneralExpensesPopupVCViewModel: GeneralExpensesPopupVCViewModelInputs, GeneralExpensesPopupVCViewModelOutputs, GeneralExpensesPopupVCViewModelType {
    
    let category: Box<String?> = Box(nil)
    let description: Box<String?> = Box(nil)
    let amount: Box<Double?> = Box(nil)

    var inputs: GeneralExpensesPopupVCViewModelInputs { return self }
    var outputs: GeneralExpensesPopupVCViewModelOutputs { return self }
}

protocol GeneralExpensesPopupVCDelegate: class {
    func generalExpensesPopupVCDidYes(
        _ vc: GeneralExpensesPopupVC,
        description: String,
        amount: Double,
        category: String
    )
    
    func generalExpensesPopupVCpDidNo(
        _ vc: GeneralExpensesPopupVC
    )
}

class GeneralExpensesPopupVC: UIViewController {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var descTextField: UITextField!
    @IBOutlet weak var amoutTextField: UITextField!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    let viewModel: GeneralExpensesPopupVCViewModelType = GeneralExpensesPopupVCViewModel()
    weak var delegate: GeneralExpensesPopupVCDelegate?
    
    @IBAction func addBtnPressed(_ sender: Any) {
        
        if let description = descTextField.text, description != "", let amount = amoutTextField.text, amount != "" , let cat = viewModel.outputs.category.value {
            delegate?.generalExpensesPopupVCDidYes(self, description: description, amount: Double(amount) ?? 0.0, category: cat)
        }
        
    }
    

    @IBAction func cancelBtnPressed(_ sender: Any) {
        delegate?.generalExpensesPopupVCpDidNo(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        setupListener()
    }


    func setupView() {
        
        addBtn.layer.cornerRadius = 20
        cancelBtn.layer.cornerRadius = 20
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = UIColor("#0058B9").cgColor
        
        descTextField.delegate = self
        amoutTextField.delegate = self
    }
    
    fileprivate func setupListener() {
                    
        viewModel.outputs.description.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let value = $0 else { return }

            
            strongSelf.descTextField.text = value
        }
        
        viewModel.outputs.amount.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let value = $0 else { return }

            
            strongSelf.descTextField.text = "\(value)"
        }
    }
}

extension GeneralExpensesPopupVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

        //validate()
        textField.resignFirstResponder()

    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {

        //validate()
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        //validate()
        return true

    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        //validate()
    }
    
}
