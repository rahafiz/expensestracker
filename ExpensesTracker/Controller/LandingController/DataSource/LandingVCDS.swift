//
//  LandingVCDS.swift
//  EpfFinder
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 09/05/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation
import Library
import UIKit

class LandingVCDS: ValueCellDataSource {
    enum Section: Int {
        case Welcome
        
    }
    
    override init() {
        super.init()
    }
    
    
    func set(welcome: LandingTVCViewModelType?) {
        let section = Section.Welcome.rawValue
        
        self.clearValues(section: section)
        
        if let welcome = welcome {
            self.set(
                values: [welcome],
                cellClass: LandingTVC.self,
                inSection: section
            )
        }
        
    }
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as LandingTVC, value as LandingTVCViewModelType):
            cell.configureWith(value: value)
        default:
            assertionFailure("Unrecognized combo: \(cell), \(value)")
        }
    }
}
