//
//  LandingTVCDS.swift
//  EpfFinder
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 09/05/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation
import Library
import UIKit


class LandingTVCDS: ValueCellDataSource {
    enum Section: Int {
        case Card
    }
    
    override init() {
        super.init()
    }
    
    
    func set(card: [LandingCVCellViewModelType]?) {
        let section = Section.Card.rawValue
        
        self.clearValues(section: section)
        
        if let card = card {
            self.set(
                values: card,
                cellClass: LandingCVCell.self,
                inSection: section
            )
        }
        
    }
    
    override func configureCell(collectionCell cell: UICollectionViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as LandingCVCell, value as LandingCVCellViewModelType):
            cell.configureWith(value: value)
        default:
            assertionFailure("")
        }
    }
    
}
