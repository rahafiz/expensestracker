//
//  LandingVC.swift
//  EpfFinder
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 09/05/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import UIKit

import Library

class LandingVC: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel: LandingViewModelType = LandingViewModel()
    var dataSource = LandingVCDS()
    
    fileprivate var cellHeightsDict = [IndexPath: CGFloat]()
    
    let addButton = UIButton()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupListener()
        setupNavBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let firstRun = Cache.getFirstRun(keyName: "first_run") {
            if firstRun {
                goToHomePage()
            }
        }
    }
    
    private func setupView() {
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none

    }
    
    private func setupNavBar() {
        
    }

    
    func setupListener() {
        
        tableView.registerCellNibForClass(LandingTVC.self)
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        
        viewModel.outputs.collection.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            DispatchQueue.main.async {
                strongSelf.dataSource.set(welcome: value)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
    }
    
    func goToHomePage() {
        let identifier = String(describing: MainVC.self)
        guard let vc = vc("Main", identifier: identifier) as? MainVC else { return }
        
        navigationController?.pushViewController(
            vc,
            animated: true
        )
    }
    
}

//MARK: Tableview Cell Delegate
extension LandingVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeightsDict[indexPath] {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDict[indexPath] = cell.frame.height
        
        if let cell = cell as? LandingTVC {
            cell.delegate = self
        }
        
    }
}

//MARK: Landing Tableview Cell Delegate
extension LandingVC: LandingTVCDelegate {
    func landingTVC(_ cell: LandingTVC) {
        Cache.setFirstRun(value: true, keyName: "first_run")
        goToHomePage()
    }
    
}
