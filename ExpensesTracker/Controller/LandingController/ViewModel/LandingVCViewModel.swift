//
//  LandingVCViewModel.swift
//  EpfFinder
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 09/05/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation

protocol LandingViewModelViewModelInputs {
  
}

protocol LandingViewModelOutputs {
   
    var collection: Box<LandingTVCViewModelType?> { get }
}

protocol LandingViewModelType {
    var inputs: LandingViewModelViewModelInputs { get }
    var outputs: LandingViewModelOutputs { get }
}

class LandingViewModel: LandingViewModelViewModelInputs, LandingViewModelOutputs, LandingViewModelType {
   
    let collection: Box<LandingTVCViewModelType?> = Box(nil)
    
    var inputs: LandingViewModelViewModelInputs { return self }
    var outputs: LandingViewModelOutputs { return self }
    
    init() {
       // Data layout injection
        let tempWelcomeCollection = LandingTVCViewModel()
        var tempWelcomeCollections = [LandingCVCellViewModel]()
        let welcomeCollection = LandingCVCellViewModel()
        welcomeCollection.outputs.animationName.value = "expenses-tracker"
        welcomeCollection.outputs.description.value = "Track your expenses professionally"

        tempWelcomeCollections.append(welcomeCollection)

        tempWelcomeCollection.outputs.welcomeCollection.value = tempWelcomeCollections
        collection.value = tempWelcomeCollection
    }
    
}
