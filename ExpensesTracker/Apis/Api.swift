//
//  Api.swift
//  EpfFinder
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 11/05/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum ApiMessageStyle: String {
    case inline = "inline"
    case overlay = "overlay"
    case none = ""
}

enum ApiError: Error {
    case objectSerialization(reason: String)
    case ruleError(
        errorCode: Int?,
        messageStyle: ApiMessageStyle,
        title: String?,
        subTitle: String?,
        reason: String?
    )
    case ruleAttachmentError(
        errorCode: Int?,
        messageStyle: ApiMessageStyle,
        title: String?,
        subTitle: String?,
        reason: String,
        messageAttachment: String?,
        imageUrl: URL?,
        messageButton: String?
    )
    case networkError(reason: String)
}

extension ApiError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .objectSerialization(reason: let reason):
            return reason
        case .ruleError(errorCode: _, messageStyle: _, title: _, subTitle: _, reason: let reason):
            return reason
        case .ruleAttachmentError(errorCode: _, messageStyle: _, title: _, subTitle: _, reason: let reason, messageAttachment: _, imageUrl: _, messageButton: _):
            return reason
        case .networkError(reason: let reason):
            return reason
        }
    }
    
    public var errorCode: Int? {
        switch self {
        case .objectSerialization(reason: _):
            return nil
        case .ruleError(errorCode: let _errorCode, messageStyle: _, title: _, subTitle: _, reason: _):
            return _errorCode
        case .ruleAttachmentError(errorCode: let _errorCode, messageStyle: _, title: _, subTitle: _, reason: _, messageAttachment: _, imageUrl: _, messageButton: _):
            return _errorCode
        case .networkError(reason: _):
            return nil
        }
    }
}

class RetryHandler: RequestRetrier, RequestAdapter {
   
    private let lock = NSLock()
    
    var defaultRetryCount = 1
    private var requestsToRetry: [(Request, RequestRetryCompletion, Int)] = []
    
    //
    private func index(request: Request) -> Int? {
        return requestsToRetry.index(where: { $0.0 === request })
    }
    
    private func addRetry(_ request: Request, completion: @escaping RequestRetryCompletion, retryCount: Int) {
        requestsToRetry.append((request, completion, retryCount))
    }
    
    private func retryAll() {
        for i: Int in 0..<requestsToRetry.count {
            let retryCount = requestsToRetry[i].2
            
            if retryCount > 0 {
                requestsToRetry[i].2 -= 1
                requestsToRetry[i].1(true, 0.0)
            } else {
                requestsToRetry[i].1(false, 0.0)
            }
        }
    }
    
    private func removeAll() {
        requestsToRetry.forEach { $0.1(false, 0.0) }
        requestsToRetry.removeAll()
    }
    
    // Intercept 403 and do a session authorization.
    // Should change to 401
    public func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse, 403 == response.statusCode, let _ = request.request {
            let requestUrlString = request.request?.url?.absoluteString ?? ""
            //BSTLogger.shared.debug("api.shouldRetry: \(requestUrlString)?")
            
            if index(request: request) == nil {
                addRetry(request,
                         completion: completion,
                         retryCount: defaultRetryCount)
            }
            
            guard let idx = index(request: request) else { completion(false, 0); return }
            let (_, _, retryCount) = requestsToRetry[idx]
            
            guard retryCount > 0 else { completion(false, 0); return }
            
            //BSTLogger.shared.debug("API session token expired, trying to renew session token: \(String(describing: response.url?.absoluteString))")
            /*_ = Api.createSessionToken({[weak self] (result) in
                guard let strongSelf = self else { return }
                strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }
                
                if result.isSuccess {
                    //BSTLogger.shared.debug("API session token renewed success: \(String(describing: response.url?.absoluteString))")
                    
                    self?.retryAll()
                } else if let error = result.error as? URLError {
                    //BSTLogger.shared.debug("API session token renewed failed: \(String(describing: response.url?.absoluteString))")
                    self?.removeAll()
                    
                    if error.code.rawValue == 403 {
//                        NotificationCenter.default.post(name: C.Notification.Logout, object: self)
                    }
                }/* else if let error = result.error as? AFError {
                    if error.responseCode == 403 {
                        NotificationCenter.default.post(name: C.Notification.Logout, object: self
                        )
                        SVProgressHUD.dismiss()
                    } else {
                        BSTLogger.shared.debug("API session token renewed failed: \(String(describing: response.url?.absoluteString))")
                    }
                }*/ else {
                    //BSTLogger.shared.debug("API session token renewed failed: \(String(describing: response.url?.absoluteString))")
                    self?.removeAll()
                }
            })*/
        } else {
            completion(false, 0.0)
        }
    }
    
    /// Sign the request with the session token.
    public func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        /*if let token = Cache.getUser(keyName: C.Cache.Token) {
            urlRequest.setValue(token,
                                forHTTPHeaderField: "Authorization")
        }*/
        
        return urlRequest
    }
}

class CustomServerTrustPolicyManager: ServerTrustPolicyManager {
    
    override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
        // Check if we have a policy already defined, otherwise just kill the connection
        if let policy = super.serverTrustPolicy(forHost: host) {
            //            BSTLogger.shared.debug(policy)
            return policy
        } else {
            return .customEvaluation({ (_, _) -> Bool in
                return false
            })
        }
    }
}

// Network Manager
public let networkManager = NetworkReachabilityManager()

class BSTSessionManager: SessionManager {
    
    override func request(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil) -> DataRequest {
        var customParameters = parameters
        
        if method == .post {
            if customParameters == nil {
                customParameters = [:]
            }
            
            if var _parameters = customParameters {
                /*if let ipAddress = IPAddrHelper.cachedIpAddr {
                    _parameters["ipAddress"] = ipAddress
                }*/
  
                /*let latitude = Location.sharedInstance().currentLatitude
                let longitude = Location.sharedInstance().currentLongitude*/
                
                /*if _parameters["latitude"] == nil && latitude != 0 {
                    _parameters["latitude"] = latitude
                }
                
                if _parameters["longitude"] == nil && longitude != 0 {
                    _parameters["longitude"] = longitude
                }
                
                customParameters = _parameters*/
            }
        }
        
        return super.request(
            url,
            method: method,
            parameters: customParameters,
            encoding: encoding,
            headers: headers
        )
    }
}

class Api {
    static let API = Api()
    
    let sessionManagerWithoutSSLPinning: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60
        
        return BSTSessionManager(
            configuration: configuration,
            serverTrustPolicyManager: nil
        )
    }()

    
    let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60
        
        //let devUrl = URLComponents.init(string: C.Hosts.dev.rawValue)
        //let stageUrl = URLComponents.init(string: C.Hosts.staging.rawValue)
        //let prodUrl = URLComponents.init(string: C.Hosts.prod.rawValue)
   
        
        //let devHost = devUrl?.host ?? ""
        //let prodHost = prodUrl?.host ?? ""
        //let stageHost = stageUrl?.host ?? ""
        
        let array = ServerTrustPolicy.certificates()
        
        /*let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "\(devHost)": .disableEvaluation,
            "\(prodHost)": .disableEvaluation,
            "\(stageHost)": .disableEvaluation
        ]*/
        
        /*let serverTrustPolicyManager = CustomServerTrustPolicyManager(
            policies: ["":nil]
        )*/
        
        //Dotzu.sharedManager.addLogger(session: configuration)

        return BSTSessionManager(
            configuration: configuration,
            serverTrustPolicyManager: nil
        )
    }()
    let acceptableStatusCodes: [Int] = {
        var arr: [Int] = []
        
        arr.append(contentsOf: 200..<403)
        arr.append(contentsOf: 404..<1000)
        
        return arr
    }()
    
    init() {
        let retrier = RetryHandler()
        
        sessionManager.adapter = retrier
        sessionManager.retrier = retrier
    }
    
    // Check network status and return error message
    static func networkRechabilityMessage(response: DefaultDataResponse) -> String? {
        switch networkManager!.networkReachabilityStatus {
        case .notReachable : return  "Network is not Reachable"
        case .reachable(.ethernetOrWiFi) : return connectionIssueMessage(response: response)
        case .reachable(.wwan) : return connectionIssueMessage(response: response)
        case .unknown : return "Network is not Reachable"
        }
    }
    
    // If any error will be there after network establishment it will return that message
    static func connectionIssueMessage(response: DefaultDataResponse) -> String? {
        if let urlError = response.error as? URLError {
            if urlError.code == .timedOut {
                return  "Connection Timed Out"
            } else if (urlError.code == .networkConnectionLost || urlError.code == .unknown || urlError.code == .cannotConnectToHost) {
                return "Something went wrong"
            }
        }
        return nil
    }
    
    static func withResult(_ response: DefaultDataResponse) -> Result<JSON> {
        return withResult(true, response)
    }
    
    static func withResult(_ checkStatus: Bool, _ response: DefaultDataResponse) -> Result<JSON> {
//        guard response.error == nil else {
//            BSTLogger.shared.debug(response.error!)
//            return .failure(response.error!)
//        }
        /*BSTLogger.shared.debug(
            "API Response \(response.timeline.totalDuration)s: \(String(describing: response.response?.url?.absoluteString)) \(String(data: response.data!, encoding: .utf8) ?? "API Response could not be printed")",
            context: "Api.Response"
        )
        BSTLogger.shared.debug(
            "API Status code: \(String(describing: response.response?.statusCode))"
        )*/
        
        guard let data: Data = response.data else {
            let errorMsg = NSLocalizedString(
                "common_error_content",
                comment: ""
            )
            
            return .failure(ApiError.objectSerialization(reason: errorMsg))
        }
        
        do {
            let json: JSON = try JSON(data: data)
            if checkStatus {
                      if response.response?.statusCode != 200 {
                          let errorCode = json["errorCode"].int ?? response.response?.statusCode
                          var error: String = {
                              if let errorStr = json["messageText"].string {
                                  return errorStr
                              } else if let errorStr = networkRechabilityMessage(response: response) {
                                  return errorStr
                              }
                              
                              return NSLocalizedString(
                                  "common_error_content",
                                  comment: ""
                              )
                          }()
                          let messageTitle: String = json["title"].string ?? NSLocalizedString(
                              "common_error_header",
                              comment: ""
                          )
                          let messageSubTitle: String = json["subtitle"].string ?? NSLocalizedString(
                              "common_error_title",
                              comment: ""
                          )
                          let imageUrl: URL? = json["imageURL"].url //maybe need to append resolutionType
                          let messageButton: String? = json["messageButton"].string
                          let messageAttachment: String = json["messageAttachment"].string ?? ""
                          let messageStyleString = json["messageStyle"].string ?? ApiMessageStyle.none.rawValue
                          let messageStyle: ApiMessageStyle = ApiMessageStyle(
                              rawValue: messageStyleString
                          ) ?? ApiMessageStyle.none
                          
                          // Message style
                          error = error.replacingOccurrences(of: "<br/>", with: "\n")
                          
                          if json["errorCode"] == 302 && (response.response?.url?.absoluteString.contains("/customer/signin"))! {
                              error = "Sign In error code 302"
                              return .failure(ApiError.objectSerialization(reason: error) )
                          } else if json["errorCode"] == 1521 && (response.response?.url?.absoluteString.contains("/transaction/cashout"))! {
                              if let message = json["messageText"].string {
                                  return .failure(ApiError.objectSerialization(reason: message) )
                              }
                          }

                          if let _ = response.error ,networkManager?.isReachable == false{
                              return .failure(
                                  ApiError.networkError(reason: error)
                              )
                          } /*else if messageStyle == .overlayWithAttachment || messageStyle == .overlayWithImage {
                              return .failure(
                                  ApiError.ruleAttachmentError(
                                      errorCode: errorCode,
                                      messageStyle: messageStyle,
                                      title: messageTitle,
                                      subTitle: messageSubTitle,
                                      reason: error,
                                      messageAttachment: messageAttachment,
                                      imageUrl: imageUrl,
                                      messageButton: messageButton
                                  )
                              )
                          }*/ else {
                              return .failure(
                                  ApiError.ruleError(
                                      errorCode: errorCode,
                                      messageStyle: .overlay,
                                      title: messageTitle,
                                      subTitle: messageSubTitle,
                                      reason: error
                                  )
                              )
                          }
                      }
                  }
                  
                  return .success(json)
            
        } catch {
           // or display a dialog
            if let _ = response.error ,networkManager?.isReachable == false{
                return .failure(
                    ApiError.networkError(reason: response.error?.localizedDescription ?? NSLocalizedString(
                            "common_error_title",
                            comment: ""
                        ))
                    )
            } else {
                return .failure(
                    ApiError.ruleError(
                        errorCode: 404,
                        messageStyle: .overlay,
                        title: NSLocalizedString(
                            "common_error_header",
                            comment: ""
                        ),
                        subTitle: NSLocalizedString(
                            "common_error_title",
                            comment: ""
                        ),
                        reason: NSLocalizedString(
                            "common_error_content",
                            comment: ""
                        )
                    )
                )
            }
        }

    }
    
    /*static func customerId() -> String {
        if let customerId: String = UserInfo.loadUser()?.CustomerId {
            return customerId
        }
        
        return ""
    }*/
    
    static func platform() -> String {
        return "ios"
    }
    
    /*static func accountId() -> String {
        let accountId = Cache.getUser(keyName: C.Cache.CustomerUniqueId) ?? ""
        
        /*if accountId.count == 0 {
            //BSTLogger.shared.debug("###WARNING: Cache.AccountId is empty")
        }*/
        
        return accountId
    }*/
    
    /*static func kycAuthHeader() -> HTTPHeaders {
        let kycToken = UserInfo.loadUser()?.KycToken as String? ?? ""
        
        if kycToken.count == 0 {
            //BSTLogger.shared.debug("###WARNING: kyctoken is empty")
        }
        
        return ["Authorization": kycToken]
    }*/
    
    static func sessionAuthHeader() -> HTTPHeaders {
        let sessionToken = ""
        if sessionToken.count == 0 {
            //BSTLogger.shared.debug("###WARNING: Cache.sessionToken is empty")
        }
        
        return ["Authorization": sessionToken]
    }
    
    /*static func createSessionToken(_ handler: @escaping (Result<String>) -> Void) -> Request {
        
        return Alamofire
            .request(
                "\(C.Host)/login",
                method: .get,
                headers: sessionAuthHeader()
            )
//            .validate()
            .response { response in
//                if let error = response.error {
//                    handler(.failure(error))
//                    return
//                }
                
                let result = withResult(response)
                if let error = result.error {
                    handler(.failure(error))
                    return
                }
                
                guard let sessionToken = result.value?["sessionToken"].string else {
                    handler(.failure(ApiError.objectSerialization(reason: "Could not retrieve session_token from JSON")))
                    return
                }
                
                if let migrationStatus = result.value?["migrationStatus"].string {
                    if (migrationStatus != C.AccMigrationStatus.Completed) {
                        NotificationCenter.default.post(name: C.Notification.ForceMigrateAccount, object: nil)
                    }
                }
                    
                Cache.setUser(value: sessionToken, keyName: C.Cache.Token)
                handler(.success(sessionToken))
        }
    }*/
    
    static func errorMessageStyleFor(error: Error) -> ApiMessageStyle? {
        var errorMessageStyle: ApiMessageStyle? = nil
        
        if let apiError = error as? ApiError {
            switch apiError {
            case .objectSerialization(reason: _):
                break
            case .ruleError(
                errorCode: _, messageStyle: let messageStyle,
                title: _, subTitle: _, reason: _):
                errorMessageStyle = messageStyle
            case .ruleAttachmentError(
                errorCode: _, messageStyle: let messageStyle,
                title: _, subTitle: _, reason: _, messageAttachment: _,
                imageUrl: _, messageButton: _):
                errorMessageStyle = messageStyle
            case .networkError(reason: _):
                break
            }
        }
        
        return errorMessageStyle
    }
}
